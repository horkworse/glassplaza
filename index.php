<!DOCTYPE html>
<html class="html">
<head>
    <title>Душевые кабины и перегородки</title>
    <meta charset="utf8">
    <meta lang="ru">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="media/images/icon.ico" type="image/x-icon">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://kenwheeler.github.io/slick/slick/slick-theme.css">
    <link rel="stylesheet" href="media/styles/slick.css">
    <link rel="stylesheet" href="media/styles/slick-theme.css">
    <link rel="stylesheet" href="media/styles/magnific.css">
    <link rel="stylesheet" type="text/css" href="media/styles/style.css">
</head>
<body class="body">
    <header class="header">
        <section class="menu__head ">
            <img src="media/images/logo.svg" alt="" class="logo header__logo">
            <aside class="menu__info">
                <a href="tel:+79261424770" class="menu__phone">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <span>+7 (926) 142 47-70</span>
                </a>
                <a href="mailto:info@glass-plaza.ru" class="menu__mail">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <span>info@glass-plaza.ru</span>
                </a>
            </aside>
        </section>
        <ul class="menu menu_top">
            <li class="menu__item"><a href="#about">О компании</a></li>
            <li class="menu__item"><a href="#type_of_shower">Виды душевых</a></li>
            <li class="menu__item"><a href="#furniture">Фурнитура</a></li>
            <li class="menu__item"><a href="#colors">Цвета</a></li>
            <li class="menu__item"><a href="#our_works">Наши работы</a></li>
            <li class="menu__item"><a href="#reviews">Отзывы</a></li>
            <li class="menu__item"><a href="#contacts">Контакты</a></li>
        </ul>
        <aside class="menu__motto">
            <h1 class="content__box menu__caption">
                Стеклянные душевые кабины на заказ с доставкой и монтажом
            </h1>
            <button class="button menu__btn">Заказать кабину</button>
        </aside>
    </header>
    <main class="main">
        <aside class="content">
            <section class="content__box types" id="type_of_shower">
                <h2 class="cpt types__cpt">Виды конструкций душевых кабин</h2>
                <ul class="types__box">
                    <li class="types__item">
                        <h3 class="types__name">Одиночная перегородка</h3>
                        <div class="types__info">
                            <a href="#content1" class="types__mark"><i class="fa fa-question" aria-hidden="true"></i></a>
                            <img src="media/images/construction-1.png" alt="" class="types__img">
                            <p id="content1" class="types__text white-popup hider">
                                Такой вариант оценят почитатели минимализма. Сдержанный дизайн одиночной перегородки, которая крепится к стене, не требует сложных архитектурных решений. При этом полотно прекрасно защищает помещение от капель влаги.
                            </p>
                        </div>
                        <span class="types__price">от 17 000 ₽</span>
                        <button class="button types__order" data-attr="Одиночная перегородка">Заказать рассчет</button>
                    </li>
                    <li class="types__item">
                        <h3 class="types__cpt">Перегородка с распашной дверью в нишу</h3>
                        <div class="types__info">
                            <a href="#content2" class="types__mark"><i class="fa fa-question" aria-hidden="true"></i></a>
                            <img src="media/images/construction-2.png" alt="" class="types__img">
                            <p id="content2" class="types__text white-popup hider">
                                Распашная стеклянная конструкция в нишу – всегда модная классика для обустройства душевых кабин. Открываться дверь может вправо и влево – все зависит от обстановки в помещении. При желании часть перегородки может быть неподвижной.
                            </p>
                        </div>
                        <span class="types__price">от 21 500 ₽</span>
                        <button class="button types__order" data-attr="Перегородка с распашной дверью в нишу">Заказать рассчет</button>
                    </li>
                    <li class="types__item">
                        <h3 class="types__cpt">Перегородка с раздвижной дверью в нишу</h3>
                        <div class="types__info">
                            <a href="#content3" class="types__mark"><i class="fa fa-question" aria-hidden="true"></i></a>
                            <img src="media/images/construction-3.png" alt="" class="types__img">
                            <p id="content3" class="types__text white-popup hider">
                                Раздвижная дверь – отличное решение для тех, кто хочет сэкономить место. Вам не нужно задумываться о том, как организовать свободное пространство для открытия створки. При этом внутри ниши, на двух стенах вы можете разместить любое оборудование.
                            </p>
                        </div>
                        <span class="types__price">от 29 000 ₽</span>
                        <button class="button types__order" data-attr="Перегородка с распашной дверью в нишу">Заказать рассчет</button>
                    </li>
                    <li class="types__item">
                        <h3 class="types__cpt">Угловая перегородка с раздвижной системой</h3>
                        <div class="types__info">
                            <a href="#content4" class="types__mark"><i class="fa fa-question" aria-hidden="true"></i></a>
                            <img src="media/images/construction-4.png" alt="" class="types__img">
                            <p id="content4" class="types__text white-popup hider">
                                Два стеклянных полотна даже в небольшой ванной комнате создадут иллюзию простора. Пространство вне душа будет надежно защищено от брызг, а разместить такую конструкцию можно в любом углу. Раздвижная дверь не помешать расставить вокруг мебель и сантехнику.
                            </p>
                        </div>
                        <span class="types__price">от 34 500 ₽</span>
                        <button class="button types__order" data-attr="Угловая перегородка с раздвижной системой">Заказать рассчет</button>
                    </li>
                    <li class="types__item">
                        <h3 class="types__cpt">Угловая перегородка с распашной дверью</h3>
                        <div class="types__info">
                            <a href="#content5" class="types__mark"><i class="fa fa-question" aria-hidden="true"></i></a>
                            <img src="media/images/construction-5.png" alt="" class="types__img">
                            <p id="content5" class="types__text white-popup hider">
                                Самое оптимальное место для расположения душа – угол комнаты. Два стеклянных полотна, которые крепятся к стенам и между собой, надежно защищают пространство от влаги. Исходя из дизайнерского проекта комнаты, распашную дверь можно расположить на любой стороне.
                            </p>
                        </div>
                        <span class="types__price">от 23 800 ₽</span>
                        <button class="button types__order" data-attr="Угловая перегородка с распашной дверью">Заказать рассчет</button>
                    </li>
                    <li class="types__item">
                        <h3 class="types__cpt">П-образная перегородка</h3>
                        <div class="types__info">
                            <a href="#content6" class="types__mark"><i class="fa fa-question" aria-hidden="true"></i></a>
                            <img src="media/images/construction-6.png" alt="" class="types__img">
                            <p id="content6" class="types__text white-popup hider">
                                П-образная конструкция из 3-х стеклянных полотен – отличная идея для тех, кто хочет сделать душевую кабину центром ванной комнаты. Размеры подбираются исходя из габаритов помещения, а систему открывания двери можно выбрать раздвижную или распашную.
                            </p>
                        </div>
                        <span class="types__price">от 31 000 ₽</span>
                        <button class="button types__order" data-attr="П-образная перегородка">Заказать рассчет</button>
                    </li>
                    <li class="types__item">
                        <h3 class="types__cpt">Перегородка на ванную</h3>
                        <div class="types__info">
                            <a href="#content7" class="types__mark"><i class="fa fa-question" aria-hidden="true"></i></a>
                            <img src="media/images/construction-7.png" alt="" class="types__img">
                            <p id="content7" class="types__text white-popup hider">
                                Элегантный экран на ванную – чудесная альтернатива шторкам. Стекло отвечает требованиям экологичности и долговечности, а значит, наслаждаться подобным решением вы будете много лет. В зависимости от ширины изделия его можно дополнить открывающейся створкой.
                            </p>
                        </div>
                        <span class="types__price">от 9 000 ₽</span>
                        <button class="button types__order" data-attr="Перегородка на ванную">Заказать рассчет</button>
                    </li>
                    <li class="types__item">
                        <h3 class="types__cpt">Трапеция</h3>
                        <div class="types__info">
                            <a href="#content8" class="types__mark"><i class="fa fa-question" aria-hidden="true"></i></a>
                            <img src="media/images/construction-8.png" alt="" class="types__img">
                            <p id="content8" class="types__text white-popup hider">
                                Шикарный вариант для любых ванных комнат – оригинальная душевая кабина-пятигранник с распашной дверью. В большом помещении она не создает иллюзию габаритного сооружения, а маленьком – визуально расширяет площадь за счет срезанного угла.
                        
                            </p>
                        </div>
                        <span class="types__price">от 24 300 ₽</span>
                        <button class="button types__order" data-attr="Трапеция">Заказать рассчет</button>
                    </li>
                </ul>
            </section>
            <section class="content__box about" id="about">
                <h2 class="cpt about__cpt">О компании</h2>
                <aside class="about__box">
                    <img src="media/images/company.jpg" alt="" class="about__img">
                    <div class="about__text">
                        <p>Компания основана в 2010 году. Мы изготавливаем душевые кабины из стекла любых размеров и под любой ценник. Мы сами проектируем, изготавливаем и монтируем душевых кабин из стекла</p>
                        <p>В настоящее время существует огромный выбор крепежных элементов для душевых. Мы используем фурнитуру только высокого качества, изготовленную из нержавеющей стали или хромированной латуни. Такие изделия, разработаны специально для эксплуатации во влажных помещениях, изготавливаются из материалов, устойчивых к ржавлению и имеет дополнительные антикоррозионные покрытия.</p>
                        <p>Мы изготавливаем различные варианты беспрофильных стеклянных душевых конструкций, которые выглядят более современно, чем аналоги с профилем</p>
                    </div>
                </aside>
            </section>
            <section class="sf__bg">
                <div class="content__box sf">
                    <aside class="sf__motto">
                        <h2 class="cpt sf__cpt">Не нашли подходящий вариант?</h2>
                        <p class="sf__text">Заполните форму, мы свяжемся с вами в ближайшее время для помощи в выборе модели</p>
                    </aside>
                    <form action="POST" class="sf__form request-form">
                        <input type="text" name="name" class="tb sf__tb" placeholder="Ваше имя">
                        <input type="text" name="phone" class="tb sf__tb" placeholder="Ваш телефон">
                        <button class="button sf__btn">Оставить заявку</button>
                        <div class="success-form">Спасибо за Ваше обращение. Мы перезвоним вам в ближайшее время.</div>
                        <p class="sf__politic">Нажимая на кнопку, вы соглашаетесь с <a href="#">политикой конфиденциальности</a></p>
                    </form>
                </div>
            </section>
            <section class="content__box works" id="our_works">
                <h2 class="cpt works__cpt">Наши работы</h2>
                <aside class="works__box">
                    <a href="media/images/portfolio/1.jpg" class="image-popup-zoom"><img src="media/images/portfolio/1.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/2.jpg" class="image-popup-zoom"><img src="media/images/portfolio/2.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/3.jpg" class="image-popup-zoom"><img src="media/images/portfolio/3.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/4.jpg" class="image-popup-zoom"><img src="media/images/portfolio/4.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/5.jpg" class="image-popup-zoom"><img src="media/images/portfolio/5.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/6.jpg" class="image-popup-zoom"><img src="media/images/portfolio/6.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/7.jpg" class="image-popup-zoom"><img src="media/images/portfolio/7.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/8.jpg" class="image-popup-zoom"><img src="media/images/portfolio/8.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/9.jpg" class="image-popup-zoom"><img src="media/images/portfolio/9.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/10.jpg" class="image-popup-zoom"><img src="media/images/portfolio/10.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/11.jpg" class="image-popup-zoom"><img src="media/images/portfolio/11.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/12.jpg" class="image-popup-zoom"><img src="media/images/portfolio/12.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/13.jpg" class="image-popup-zoom"><img src="media/images/portfolio/13.jpg" alt="" class="works__img"></a>
                    <a href="media/images/portfolio/14.jpg" class="image-popup-zoom"><img src="media/images/portfolio/14.jpg" alt="" class="works__img"></a>
                </aside>
            </section>
            <section class="sf__bg">
                <aside class="content__box frbl" id="furniture">
                    <h2 class="cpt frbl__cpt">Огромный ассортимент фурнитуры: на любой вкус и кошелек</h2>
                    <div class="container">
                        <div class="nav nav-tabs frbl__links justify-content-center" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Стандарт</a>
                            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Премиум</a>
                            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Люкс</a>
                        </div>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade frbl__box show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div class="frbl__imgs">
                                    <img src="media/images/norm/furniture_norm-1.png" alt="">
                                    <img src="media/images/norm/furniture_norm-2.png" alt="">
                                    <img src="media/images/norm/furniture_norm-3.png" alt="">
                                    <img src="media/images/norm/furniture_norm-4.png" alt="">
                                    <img src="media/images/norm/furniture_norm-5.png" alt="">
                                    <img src="media/images/norm/furniture_norm-6.png" alt="">
                                    <img src="media/images/norm/furniture_norm-7.png" alt="">
                                    <img src="media/images/norm/furniture_norm-8.png" alt="">
                                </div>
                                <p class="frbl__text">
                                    Фурнитура отечественного производителя ТМ «Титан» отвечает всем требованиям, предъявляемым к качеству изделий для душевых кабин. При этом элементы для стеклянных конструкций имеют стильный внешний вид и доступную стоимость.
                                </p>
                            </div>
                            <div class="tab-pane fade frbl__box" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <div class="frbl__imgs">
                                    <img src="media/images/prem/furniture_standart-1.jpg" alt="">
                                    <img src="media/images/prem/furniture_standart-2.jpg" alt="">
                                    <img src="media/images/prem/furniture_standart-3.jpg" alt="">
                                    <img src="media/images/prem/furniture_standart-4.jpg" alt="">
                                    <img src="media/images/prem/furniture_standart-5.jpg" alt="">
                                    <img src="media/images/prem/furniture_standart-6.jpg" alt="">
                                    <img src="media/images/prem/furniture_standart-7.jpg" alt="">
                                </div>
                                <p class="frbl__text">
                                    Немецкая фурнитура Bohle – это высокая надежность раздвижных систем, петель и других элементов стеклянных конструкций по приемлемой цене. Со временем изделия не теряют эстетичности, а при необходимости легко меняется за счет продуманного дизайна.
                                </p>
                            </div>
                            <div class="tab-pane fade frbl__box" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <div class="frbl__imgs">
                                    <img src="media/images/lux/furniture_lux-1.jpg" alt="">
                                    <img src="media/images/lux/furniture_lux-2.jpg" alt="">
                                    <img src="media/images/lux/furniture_lux-3.jpg" alt="">
                                    <img src="media/images/lux/furniture_lux-4.jpg" alt="">
                                </div>
                                <p class="frbl__text">
                                    Продукция бренда Pauli & Sohn GmbH изготовлена по инновационной технологии с применением легированного цинкового сплава. Работоспособность фурнитуры из Германии в 4 раза превышает нормативы, что подтверждено сертификатами качества и проведенных испытаний.
                                </p>
                            </div>
                        </div>
                    </div>  
                </aside>
            </section>
            <section class="content__box furniture">
                <h2 class="cpt furniture__cpt">Цвета Фурнитуры </h2>
                <ul class="furniture__box">
                    <li class="furniture__item">
                        <img src="media/images/color_furniture-1.png" alt="" class="furniture__img">
                        <span class="furniture__name">Хром</span>
                    </li>
                    <li class="furniture__item">
                        <img src="media/images/color_furniture-2.png" alt="" class="furniture__img">
                        <span class="furniture__name">Матовая сталь</span>
                    </li>
                    <li class="furniture__item">
                        <img src="media/images/color_furniture-3.png" alt="" class="furniture__img">
                        <span class="furniture__name">Золото</span>
                    </li>
                    <li class="furniture__item">
                        <img src="media/images/color_furniture-4.png" alt="" class="furniture__img">
                        <span class="furniture__name">Бронза</span>
                    </li>
                    <li class="furniture__item">
                        <img src="media/images/color_furniture-5.png" alt="" class="furniture__img">
                        <span class="furniture__name">Черная матовая</span>
                    </li>
                </ul>
            </section>
            <section class="content__box colors" id="colors">
                <h2 class="cpt colors__cpt">Цвета</h2>
                <p class="colors__text">
                    Душевая кабина – сверхинтимное пространство, поэтому далеко не каждый хочет видеть в ванной комнате полностью прозрачные перегородки. Мы заботимся о том, чтобы банные процедуры принесли вам максимальный релакс, поэтому предлагаем матированные и тонированные стекла в разных вариантах исполнения. Выбирайте оттенок под интерьер и вкусовые предпочтения.
                </p>
                <ul class="colors__box">
                    <li class="colors__item">
                        <img src="media/images/colors/clear-glass1.png" alt="" class="colors__img">
                        <p class="colors__desc">Прозрачное</p>
                    </li>
                    <li class="colors__item">
                        <img src="media/images/colors/HD-glass1.png" alt="" class="colors__img">
                        <p class="colors__desc">Остветленное</p>
                    </li>
                    <li class="colors__item">
                        <img src="media/images/colors/opaque-clear-glass1.png" alt="" class="colors__img">
                        <p class="colors__desc">Прозрачное матовое</p>
                    </li>
                    <li class="colors__item">
                        <img src="media/images/colors/gray-glass1.png" alt="" class="colors__img">
                        <p class="colors__desc">Серое</p>
                    </li>
                    <li class="colors__item">
                        <img src="media/images/colors/bronze-glass1.png" alt="" class="colors__img">
                        <p class="colors__desc">Бронзовое</p>
                    </li>
                </ul>
            </section>
            <section class="sf__bg">
                <div class="content__box sfl">
                    <h2 class="cpt sfl__cpt">Оставьте заявку и получите -15% к изделию</h2>
                    <form action="POST" class="sfl__lf request-form">
                        <input type="text" name="name" class="tb sf__tb" placeholder="Ваше имя">
                        <input type="text" name="phone" class="tb sf__tb" placeholder="Ваш телефон">
                        <input type="text" name="email" class="tb sf__tb" placeholder="Ваш e-mail">
                        <button class="button sf__btn sfl__btn">Оставить заявку</button>
                        <div class="success-form">Спасибо за Ваше обращение. Мы перезвоним вам в ближайшее время.</div>
                        <p class="sf__politic">Нажимая на кнопку, вы соглашаетесь с <a href="#">политикой конфиденциальности</a></p>
                    </form>
                </div>
            </section>
            <!-- <section class="content__box reviews" id="reviews">
                <h2 class="cpt reviews__cpt">Отзывы</h2>
                <aside class="reviews__box" data-slick='{"slidesToShow": 1, "slidesToScroll": 1}'>
                    <div class="reviews__item">
                        <h3 class="reviews__name">Зубенко Василий Петрович</h3>
                        <p class="reviews__text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque velit est, maximus sed massa id, varius hendrerit ex. Duis ornare volutpat rhoncus. Pellentesque dapibus turpis massa, quis vestibulum lorem pharetra ac. Nullam porta eu nulla et varius. 
                        </p>
                    </div>
                    <div class="reviews__item">
                        <h3 class="reviews__name">Зубенко Василий Петрович</h3>
                        <p class="reviews__text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque velit est, maximus sed massa id, varius hendrerit ex. Duis ornare volutpat rhoncus. Pellentesque dapibus turpis massa, quis vestibulum lorem pharetra ac. Nullam porta eu nulla et varius. 
                        </p>
                    </div>
                    <div class="reviews__item">
                        <h3 class="reviews__name">Зубенко Василий Петрович</h3>
                        <p class="reviews__text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque velit est, maximus sed massa id, varius hendrerit ex. Duis ornare volutpat rhoncus. Pellentesque dapibus turpis massa, quis vestibulum lorem pharetra ac. Nullam porta eu nulla et varius. 
                        </p>
                    </div>
                </aside>
            </section> -->
        </aside>
        <div class="form-popup mfp-hide white-popup-block">
            <div class="white-popup-block">
                <div class="container">
                    <form class="request-form" method="POST" onsubmit="yaCounter50202622.reachGoal('form_zajavka'); return true;">
                        <h3>Душевая кабина из стекла на заказ</h3>
                        <p>Укажите Ваш телефон, мы свяжемся с вами в ближайшее время</p>
                        <input type="hidden" class="tb" name="utm_source" value="<?php echo isset($_GET['utm_source']) ? $_GET['utm_source'] : '' ;?>">
                        <input type="hidden" class="tb" name="utm_medium" value="<?php echo isset($_GET['utm_medium']) ? $_GET['utm_medium'] : '' ;?>">
                        <input type="hidden" class="tb" name="utm_campaign" value="<?php echo isset($_GET['utm_campaign']) ? $_GET['utm_campaign'] : '' ;?>">
                        <input type="hidden" class="tb" name="utm_term" value="<?php echo isset($_GET['utm_term']) ? $_GET['utm_term'] : '' ;?>">
                        <input type="hidden" class="tb" name="utm_content" value="<?php echo isset($_GET['utm_content']) ? $_GET['utm_content'] : '' ;?>">
                        <input type="text" class="tb" name="name" placeholder="Ваше имя" required>
                        <input type="text" class="tb" id="phone" name="phone" placeholder="Ваш телефон" required>
                        <input type="text" name="attr" class="hide-input-attr">
                        <button type="submit" class="button btn-popup-form" >Отправить</button>
                        <div class="success-form success-form-popup">Спасибо за Ваше обращение. Мы перезвоним вам в ближайшее время.</div>
                    </form>
                </div>
            </div>
        </div>
    </main>
    <footer class="footer">
        <section class="content__box contacts" id="contacts">
                <h2 class="cpt contacts__cpt">Контакты</h2>
                <aside class="contacts__links">
                    <div class="contacts__block">
                        <span class="contacts__icon">
                            <i class="fa fa-mobile fa-3x" aria-hidden="true"></i>
                        </span>
                        <p class="contacts__info">
                            <a href="tel:+79261424770" class="contacts__cpt">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span>+7 (926) 142 47-70</span>
                            </a>
                            <span class="contacts__text">ЗВОНИТЕ, ПИШИТЕ В WHATSAPP, VIBER, TELEGRAM C 9 ДО 21</span>
                        </p>
                    </div>
                    <div class="contacts__block">
                        <span class="contacts__icon">
                            <i class="fa fa-envelope fa-3x" aria-hidden="true"></i>
                        </span>
                        <p class="contacts__info">
                            <a href="mailto:info@glass-plaza.ru" class="contacts__cpt">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <span>info@glass-plaza.ru</span>
                            </a>
                            <span class="contacts__text">ОТВЕЧАЕМ НА ЭЛЕКТРОННЫЕ ПИСЬМА В ТЕЧЕНИЕ СУТОК</span>
                        </p>
                    </div>
                    <div class="contacts__block">
                        <span class="contacts__icon">
                            <i class="fa fa-map-marker fa-3x" aria-hidden="true"></i>
                        </span>
                        <p class="contacts__info">
                            <span class="contacts__cpt">г. Москва, Дмитровское шоссе, дом 157, строение 12</span>
                            <span class="contacts__text">ВХОД ПО ПРЕДЪЯВЛЕНИЮ ДОКУМЕНТА УДОСТОВЕРЯЮЩЕГО ЛИЧНОСТЬ С 9 ДО 21</span>
                        </p>
                    </div>
                </aside>
                <aside class="contacts__cont">
                    <div class="contacts__map" id="map"></div>
                </aside>
                <aside class="contacts__other">
                    <img src="media/images/logo.svg" alt="" class="logo footer__logo">
                    <a href="#" class="contacts_potics">Политика конфиденциальности</a>
                </aside>
            </section>
    </footer>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- <script src="https://kenwheeler.github.io/slick/slick/slick.js"></script> -->
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="media/scripts/slick.min.js"></script>
    <script src="media/scripts/magnific.min.js"></script>
    <script src="media/scripts/script.js"></script>
</body>
</html>