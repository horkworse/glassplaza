"use strict";

$(document).ready(function(){

	$('.types__mark').magnificPopup({
        type: 'inline'
    });

    $('.menu__item a').click( function(){
	let scrollEl = $(this).attr('href');
        if ($(scrollEl).length != 0)
	    	$('html, body').animate({ scrollTop: $(scrollEl).offset().top - 70 }, 500);
	    return false;
	});

	if (window.innerWidth < 768) {
		// $('.types__text').addClass("mfp-hide");
	}
	
	$('button.types__order').each(function(){
		$(this).click(function(){
			$.magnificPopup.open({
				items: {
					src: $('.form-popup').html(),
					type: 'inline',
					preloader: false,
					focus: '#name',
				}
			});

			$('.success-form-popup').css('display','none');
			$('.btn-popup-form').prop('disabled',false);

			// attr = $(this).attr('data-attr');
			// $('.hide-input-attr').val(attr);                
			$(".request-form").on('submit', function(e) {
				e.preventDefault();

				var form_data = $(this).serialize();
				$.post('/request-form.php', form_data, function(data){
					$('.success-form-popup').css('display','block');
					$('.btn-popup-form').prop('disabled',true);
				});
			});

		});
	});
	$('button.menu__btn').each(function(){
		$(this).click(function(){
			$.magnificPopup.open({
				items: {
					src: $('.form-popup').html(),
					type: 'inline',
					preloader: false,
					focus: '#name',
				}
			});
			// attr = $(this).attr('data-attr');
			// $('.hide-input-attr').val(attr);                
			$(".request-form").on('submit', function(e) {
				e.preventDefault();

				var form_data = $(this).serialize();
				$.post('/request-form.php', form_data, function(data){
					$('.success-form-popup').css('display','block');
					$('.btn-popup-form').prop('disabled',true);
				});
			});

		});
	});

	function hundler(e) {
		e.preventDefault();
		$(this).children('.success-form').css('display','block');
		$(this).children('.sf__btn').prop('disabled',true);
		var form_data = $(this).serialize();
		$.post('/request-form.php', form_data, function(data){});
	}

	$(".sfl__lf").on('submit', hundler);
	$(".sf__form").on('submit', hundler);
});

$(window).resize(() => {
	// if (window.innerWidth < 768)
	// 	$('.types__text').addClass("mfp-hide");
	// else
	// 	$('.types__text').removeClass("mfp-hide");
})


let nav = document.getElementsByClassName('menu')[0];
let header = document.getElementsByClassName('menu__head ')[0];
let offsetValue = nav.offsetTop;

window.addEventListener('scroll', () => {
	if (window.scrollY > nav.offsetTop && window.innerWidth > 768) {
		if (nav.style.position !== 'fixed') {
			nav.style.position = 'fixed';
			nav.style.top = '0';
			nav.style.width = '100%';
			nav.style.zIndex = '1000';
			nav.style.marginTop = '0';
			header.style.paddingTop = '63px';
		}
	}
	if (window.scrollY < offsetValue) {
		if (nav.style.position === 'fixed') {
			nav.style.position = '';
			header.style.paddingTop = '0px';
		}
	}
});

$('.works__box').slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	autoplay: true,
	autoplaySpeed: 2000,
	accessibility: true,
	nextArrow: '<img src="media/images/arrow_right.png" class="works__arrow right">',
    prevArrow: '<img src="media/images/arrow_left.png" class="works__arrow left">',
    responsive: [
        {
            breakpoint: 2000,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
            }   
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
            }   
        },

    ]
});

$(".frbl__imgs").slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	autoplay: true,
	autoplaySpeed: 2000,
	accessibility: true,
	arrows: false,
	dots: true,
	adaptiveHeight: true
});


$('.nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
	e.target
	e.relatedTarget
	$('.frbl__imgs').slick('setPosition');
});

$('.reviews__box').slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	autoplay: true,
	autoplaySpeed: 2000,
	accessibility: true,
	arrows: false,
	dots: false
});

$('.image-popup-zoom').magnificPopup({
	type: 'image',
	zoom: {
		enabled: true,
		duration: 300 // продолжительность анимации. Не меняйте данный параметр также и в CSS
	}
});

ymaps.ready(init);
let map,
	mark;

function init(){	 
	map = new ymaps.Map("map", {
		center: [55.8995689, 37.5423479],
		zoom: 17
	});

	mark = new ymaps.Placemark(map.getCenter(), { 
		hintContent: 'Мы тут!', 
		balloonContent: 'Ул.Сталеваров 7/2' 
	});

	map.geoObjects.add(mark);
}